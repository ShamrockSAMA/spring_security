package com.spring.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spring.security.entity.UserEntity;

public interface UserMapper extends BaseMapper<UserEntity> {
}
