package com.spring.security.service.impl;

import com.spring.security.entity.UserEntity;
import com.spring.security.mapper.UserMapper;
import com.spring.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public UserEntity getUserByUserName(String userName) {
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("userName",userName);
        List<UserEntity> userEntityList = userMapper.selectByMap(paramMap);
        return userEntityList.get(0);
    }
}
