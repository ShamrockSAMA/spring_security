package com.spring.security.service;

import com.spring.security.entity.UserEntity;

public interface UserService {
    UserEntity getUserByUserName(String userName);
}
