package com.spring.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@TableName(value = "user")
public class UserEntity {
    @TableField(value = "userName")
    private String userName;
    @TableField(value = "userPassword")
    private String userPassword;
    @TableField(value = "userGender")
    private String userGender;
    @TableField(value = "userEmail")
    private String userEmail;
    @TableField(value = "isDelete")
    private Integer isDelete;
}
