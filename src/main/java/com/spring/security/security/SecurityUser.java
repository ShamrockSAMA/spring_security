package com.spring.security.security;

import com.spring.security.entity.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class SecurityUser extends UserEntity implements UserDetails {
    public SecurityUser(UserEntity user){
        if(null != user){
            this.setUserName(user.getUserName());
            this.setUserEmail(user.getUserEmail());
            this.setUserGender(user.getUserGender());
            this.setUserPassword(user.getUserPassword());
        }
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        String userName = this.getUserName();
        if(userName != null){
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(userName);
            authorities.add(simpleGrantedAuthority);
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return this.getUserPassword();
    }

    @Override
    public String getUsername() {
        return this.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        // 账号是否未过期,过期无法验证
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // 指定的用户是否解锁,锁定的用户无法验证
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // 指示是否已过期的用户凭据(密码),过期的凭据防止认证
        return true;
    }

    @Override
    public boolean isEnabled() {
        // 是否可用,禁用的用户不能身份验证
        return true;
    }
}
