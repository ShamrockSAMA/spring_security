DROP DATABASE IF EXISTS `family_cinema_db`;
CREATE DATABASE `family_cinema_db` CHARSET=utf8mb4;
USE family_cinema_db;
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) DEFAULT NULL COMMENT '用户名',
  `userPassword` varchar(100) DEFAULT NULL COMMENT '密码',
  `userGender` char(1) DEFAULT NULL COMMENT '性别',
  `userEmail` varchar(100) DEFAULT NULL COMMENT '邮箱地址,用于找回密码',
  `isDelete` int(11) DEFAULT NULL COMMENT '是否删除',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
INSERT INTO `user`(userName,userPassword,userGender,userEmail,isDelete)
VALUES ('小惠','$2a$04$petEXpgcLKfdLN4TYFxK0u8ryAzmZDHLASWLX/XXm8hgQar1C892W','女','303442009@qq.com',0)
